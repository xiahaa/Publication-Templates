# Intro
This folder contains the Latex template for the conference ION GNSS+.

## How to use
1. If you use overleaf, then just: New Project -> Upload Project -> Select this zip file. Overleaf will do the rest for you. Once done, you can see the template and tyoe your context.
2. If you use TexMaker, TexStudio or others, you can unzip this zip file. Then open the file called xxx-template.tex using your Latex editor. Build it, you will see the template and tyoe your context.
