# Intro

This folder contains the word template for the Journal GPS Solutions.

## Contents
1. template.doc: word template for the manuscript.
2. Three word files (84, 129, 174 mm) for easy font size verification. 

## Why and How to use the font size verification file

### Why
GPSS requires that the font, no matter in main text or in figures, should be consistent, 10pt Arial Sans. 
GPSS also supports 3 different sizes of figures in the final version, 84mm, 129mm, 174mm.
Therefore, the editor will ask the author to copy and paste figures into the corresponding file and then visually check the font size.

### How to use
1. list your figures and insert them to the corresponding word file, i.e. 84mm figure to 84mm verification file, etc.
2. check if the font appeared in the figure has a similar size with the font in the text.