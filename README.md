# Objective
This repository aims to store and maintain the templates of journals and conferences that are relevant to the interests of GEO group at DTU Space. 

## Usage
Download the template and then use it.

## Journal Included
1. GPS Solutions: word template and 3 verification files to check whether or not the font type, size, etc in figures be consistent with the font in text.

## Conference Included
1. ION GNSS+: latex template which has been adjusted according to the guidelines of ION GNSS+. This template is used by ION GNSS from 2019. 

# Contribution
We hope people can contribute relevant templates to this repository. 
To make the template easy to use, please follow the following instructions (especially for Word template):
1. make the font type, font size correctly so that others can type without the need to recheck the guidelines of a journal or a conference.
2. please at least show some basic examples on how to format the section, subsection, subsubsection, paragraph, etc.
3. add at least one example on how to insert and format a figure, how to refer the figure in the context.
4. add at least one example on how to insert and format a table, how to refer the table in the context.
5. please give some examples on how to ciate a paper, e.g. cite a paper with 1 author, 2 authors, 3 and more authors.
6. **Create a new folder, change the readme by specifying what have been placed in the folder**.

